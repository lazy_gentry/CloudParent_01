package com.cloud.service.impl;

import com.cloud.dao.ResumeDao;
import com.cloud.pojo.Resume;
import com.cloud.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

/**
 * @author Administrator
 * @version 1.0
 * @description: TODO
 * @date 2022/7/8 14:51
 */
@Service
public class ResumeServiceImpl implements ResumeService {
    @Autowired
    ResumeDao resumeDao;
    @Override
    public Resume findDefaultResumeByUserId(Long userId) {

        Resume resume=new Resume();
        resume.setUserId(userId);
        resume.setIsDefault(1);
        Example<Resume> example=Example.of(resume);
        return resumeDao.findOne(example).get();
    }
}
