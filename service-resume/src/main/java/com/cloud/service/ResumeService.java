package com.cloud.service;

import com.cloud.pojo.Resume;

/**
 * @author Administrator
 * @version 1.0
 * @description: TODO
 * @date 2022/7/8 14:50
 */

public interface ResumeService {
    Resume findDefaultResumeByUserId(Long userId);
}
