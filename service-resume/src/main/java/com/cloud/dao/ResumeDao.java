package com.cloud.dao;

import com.cloud.pojo.Resume;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Administrator
 * @version 1.0
 * @description: TODO
 * @date 2022/7/8 14:49
 */
public interface ResumeDao extends JpaRepository<Resume,Long> {
}
