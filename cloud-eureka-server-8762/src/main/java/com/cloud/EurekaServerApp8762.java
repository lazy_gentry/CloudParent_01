package com.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author Administrator
 * @version 1.0
 * @description: TODO
 * @date 2022/7/8 21:57
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServerApp8762 {
    public static void main(String[] args) {  SpringApplication.run(EurekaServerApp8762.class,args);  }
}
