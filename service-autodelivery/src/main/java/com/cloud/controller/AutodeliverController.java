package com.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author Administrator
 * @version 1.0
 * @description: TODO
 * @date 2022/7/8 16:44
 */
@RestController
@RequestMapping("/autodeliver")
public class AutodeliverController {

    // /autodeliver/checkState/{userId}
    @Autowired
    private RestTemplate restTemplate;
    @GetMapping("/checkState/{userId}")
    public Integer findResumeOpenSate(@PathVariable Long userId){
        //调用远程服务 调用 resume接口 使用httpclient要封装很多内容
        //这是get请求 还有postForObject 当然也可以通过exchange来获得更多请求
        Integer forObject = restTemplate.getForObject("http://localhost:8080/resume/openstate/" + userId, Integer.class);
        return forObject;
    }
}
