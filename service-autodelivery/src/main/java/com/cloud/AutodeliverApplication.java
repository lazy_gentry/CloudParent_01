package com.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @author Administrator
 * @version 1.0
 * @description: TODO
 * @date 2022/7/8 16:49
 */
@SpringBootApplication
public class AutodeliverApplication {
    public static void main(String[] args) {
        SpringApplication.run(AutodeliverApplication.class,args);
    }
    //使用RestTemplate模板对象进行远程调用
    @Bean
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }
}
